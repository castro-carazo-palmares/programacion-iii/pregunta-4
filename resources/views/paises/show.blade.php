@extends('layout')

@section('title', 'Mostrar País')

@section('content')

<a href={{ route('paises.edit', $pais) }}>Editar</a>

<form action="{{ route('paises.destroy', $pais)}}" method="POST">
    @csrf
    @method('DELETE')

    <input type="submit" value="Eliminar">
</form>

<h1>{{ $pais->nombre }}</h1>

<p>Casos confirmados: {{ $pais->confirmados }}</p>

<p>Personas recuperadas: {{ $pais->recuperados }}</p>

<p>Casos críticos: {{ $pais->criticos }}</p>

<p>Personas fallecidas: {{ $pais->fallecidos }}</p>

<a href={{ route('paises.index') }}>Volver</a>

@endsection
