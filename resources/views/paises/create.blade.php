@extends('layout')

@section('title', 'Crear País')

@section('content')

<h1>Agregar país</h1>

@if($errors->any())

<ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
</ul>

@endif

<div class="form">
    <form action="{{ route('paises.store') }}" method="POST">
        @csrf

        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" required>

        <label for="confirmados">Casos confirmados</label>
        <input type="number" name="confirmados" required>

        <label for="recuperados">Personas recuperadas</label>
        <input type="number" name="recuperados" required>

        <label for="criticos">Casos críticos</label>
        <input type="number" name="criticos" required>

        <label for="fallecidos">Personas fallecidas</label>
        <input type="number" name="fallecidos" required>

        <input type="submit" value="Enviar">
        <a href={{ route('paises.index') }}>Volver</a>


    </form>
</div>

@endsection
