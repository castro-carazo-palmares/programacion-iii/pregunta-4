@extends('layout')

@section('title', 'Editar País')

@section('content')

<h1>Editar país</h1>

<div class="form">
    <form action="{{ route('paises.update', $pais) }}" method="POST">
        @csrf
        @method('PATCH')

        <label for="id">ID</label>
        <input type="number" name="id" value="{{ $pais->id }}" readonly>

        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" value="{{ $pais->nombre }}" required>

        <label for="confirmados">Casos confirmados</label>
        <input type="number" name="confirmados" value="{{ $pais->confirmados }}" required>

        <label for="recuperados">Personas recuperadas</label>
        <input type="number" name="recuperados" value="{{ $pais->recuperados }}" required>

        <label for="criticos">Casos críticos</label>
        <input type="number" name="criticos" value="{{ $pais->criticos }}" required>

        <label for="fallecidos">Personas fallecidas</label>
        <input type="number" name="fallecidos" value="{{ $pais->fallecidos }}" required>

        <input type="submit" value="Enviar">
        <a href={{ route('paises.show', $pais) }}>Volver</a>


    </form>
</div>


@endsection
