@extends('layout')

@section('title', 'Paises confirmados')

@section('content')

<h1>Países confirmados</h1>

<a href="{{ route('paises.create')}}">Agregar país</a>

<ul>

    @forelse ($paises as $pais)
    <li>
        <a href="{{ route('paises.show', $pais )}}">
            {{ $pais->nombre }}
        </a>
    </li>
    @empty
    <li>No hay países confirmados</li>
    @endforelse

</ul>


@endsection
