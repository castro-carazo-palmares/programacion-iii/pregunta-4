<?php

use Illuminate\Database\Seeder;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pais')->insert([
            'nombre' => 'Costa Rica',
            'confirmados' => 347,
            'recuperados' => 4,
            'criticos' => 8,
            'fallecidos' => 2
        ]);
    }
}
