<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $fillable = ['nombre', 'confirmados', 'recuperados', 'criticos', 'fallecidos'];
}
