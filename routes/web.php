<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/paises', 'PaisController@index')->name('paises.index');
Route::get('/paises/crear', 'PaisController@create')->name('paises.create');
Route::get('/paises/{pais}/editar', 'PaisController@edit')->name('paises.edit');
Route::patch('/paises/{pais}', 'PaisController@update')->name('paises.update');
Route::post('/paises', 'PaisController@store')->name('paises.store');
Route::get('/paises/{pais}', 'PaisController@show')->name('paises.show');
Route::delete('/paises/{pais}', 'PaisController@destroy')->name('paises.destroy');
